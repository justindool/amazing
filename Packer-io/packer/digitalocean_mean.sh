#!/bin/bash

args=("$@")

#echo Number of arguments: $#
#echo 1st argument: ${args[0]}

#SAFEUSER=${args[0]}
#SAFEPASS=${args[1]}

#delete mean directory
echo "*** Deleting MEAN ***"
cd /opt
rm -rf -- mean

#create Safe User and Password
echo "Creating Safe User ***"
useradd -s /bin/bash -m -d /home/$SAFEUSER -c "user name hint" -p $(openssl passwd -1 $SAFEPASS) $SAFEUSER

#Give Safe User Permission To Use Port 80:
echo "*** Setup Port 80 for Safe User ***"
apt-get install libcap2-bin
setcap cap_net_bind_service=+ep /usr/local/bin/node

#Install PM2:
echo "*** Installing PM2 ***"
npm install pm2 -g

#Start Application (in application folder):
echo "*** Starting node (will not work without app.js) ***"
#pm2 start app.js

#Specify the platform, to start on boot (see REF-2):
echo "*** Specify the platform, to start on boot (must have app.js) ***"
#pm2 startup ubuntu
echo old value:

#follow output from command above, might need to be loged in as root, worked w/out for me (see REF-2)
echo "*** Setting Nodejs to run at boot! (must have app.js) ***"
#su -c "env PATH=$PATH:/usr/local/bin pm2 startup ubuntu -u $SAFEUSER"

echo
echo " ***  YOUR MEAN IMAGE IS MADE!. "
echo
